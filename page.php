<?php $page_id = get_the_ID(); ?>
<style>
	.gallery-preview hr, .video-preview hr {
		background-color: <?php the_field('color_two', $page_id) ?>;
	}
	.gallery-preview a, .video-preview a {
		color: <?php the_field('color_one', $page_id) ?>;
	}
	.gallery-preview a svg, .video-preview a svg {
		fill: <?php the_field('color_one', $page_id) ?>;
	}
</style>

<?php get_header(); ?>

<?php get_template_part('template-parts/sections/title'); ?>

<nav class="year-nav">
	<a href="#photos" class="smoothScroll">
		Photos
		<div class="gradient" style="background: linear-gradient(129deg, <?php the_field('color_one'); ?> 10%, <?php the_field('color_two'); ?> 70%);"></div>
	</a>
	<a href="#videos" class="smoothScroll">
		Videos
		<div class="gradient" style="background: linear-gradient(129deg, <?php the_field('color_one'); ?> 10%, <?php the_field('color_two'); ?> 70%);"></div>
	</a>
	<a href="#posts" class="smoothScroll">
		Articles
		<div class="gradient" style="background: linear-gradient(129deg, <?php the_field('color_one'); ?> 10%, <?php the_field('color_two'); ?> 70%);"></div>
	</a>
	<a href="https://f2conference.com/<?php the_field('conference_year'); ?>-recap/#speakers">
		Speakers
		<div class="gradient" style="background: linear-gradient(129deg, <?php the_field('color_one'); ?> 10%, <?php the_field('color_two'); ?> 70%);"></div>
	</a>
</nav>

<?php get_template_part('template-parts/sections/article'); ?>

<?php 
	$year = get_field('conference_year');
	$args = array(
		'post_type' => array('gallery'),
		'tax_query' => array(
			array(
					'taxonomy' => 'category',
					'field' => 'slug',
					'terms' => $year
			)
		),
		'posts_per_page' => 1,
		'nopaging' => true,
		'ignore_sticky_posts' => true,
		'order' => 'DESC',
	);
	$gallery_query = new WP_Query( $args );
?>
<?php if ( $gallery_query->have_posts() ) : ?>
	<?php while ( $gallery_query->have_posts() ) : $gallery_query->the_post(); ?>
		<a id="photos" class="anchor-point"></a>
		<?php get_template_part('template-parts/preview/photos'); ?>
	<?php endwhile; ?>
<?php endif; ?> 
<?php wp_reset_postdata(); ?>

<?php 
	$year = get_field('conference_year');
	$args = array(
		'post_type' => array('video'),
		'tax_query' => array(
			array(
					'taxonomy' => 'category',
					'field' => 'slug',
					'terms' => $year
			)
		),
		'posts_per_page' => 1,
		'nopaging' => true,
		'ignore_sticky_posts' => true,
		'order' => 'DESC',
	);
	$gallery_query = new WP_Query( $args );
?>
<?php if ( $gallery_query->have_posts() ) : ?>
	<?php while ( $gallery_query->have_posts() ) : $gallery_query->the_post(); ?>
		<a id="videos" class="anchor-point"></a>
		<?php get_template_part('template-parts/preview/videos'); ?>
	<?php endwhile; ?>
<?php endif; ?> 
<?php wp_reset_postdata(); ?>	

<?php 
	$year = get_field('conference_year');
	$args = array(
		'post_type' => array('post'),
		'tax_query' => array(
			array(
					'taxonomy' => 'category',
					'field' => 'slug',
					'terms' => $year
			)
		),
		'posts_per_page' => 3,
		'nopaging' => true,
		'ignore_sticky_posts' => true,
		'order' => 'DESC',
	);
	$gallery_query = new WP_Query( $args );
?>
<?php if ( $gallery_query->have_posts() ) : ?>
<a id="posts" class="anchor-point"></a>
	<section class="posts">	
		<?php if ( is_page() || is_single() ) : ?>	
			<h2>PR & Articles</h2>
		<?php else : ?>
			<h2><?php the_title(); ?></h2>
		<?php endif; ?>
		<a href="/category/<?php the_field('conference_year'); ?>/" style="color: <?php the_field('color_one') ?>;">
			See all articles
			<svg width="9" height="14" viewBox="0 0 9 14"><path fill="<?php the_field('color_one') ?>" d="M8.36 7.87l-6.43 5.29c-.56.45-1.32.3-1.71-.34a1.65 1.65 0 0 1-.22-.82v-10.58c0-.78.55-1.42 1.22-1.42.25 0 .5.09.71.26l6.43 5.29c.55.45.68 1.34.29 1.98a1.2 1.2 0 0 1-.29.34z"></path></svg>
		</a>
		<hr style="background-color: <?php the_field('color_two') ?>;">
		<?php while ( $gallery_query->have_posts() ) : $gallery_query->the_post(); ?>
			<?php get_template_part('template-parts/preview/posts'); ?>
		<?php endwhile; ?>
	</section>
<?php endif; ?> 
<?php wp_reset_postdata(); ?>

<?php get_footer(); ?>