<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

	<div class="min-height">
		<article class="post">

			<h2><?php the_title(); ?></h2>
			<hr style="background-color: <?php the_field('color_two', $page_id) ?>;">

			<article class="editor-contents">
				<?php the_post_thumbnail('large'); ?>
				<?php the_content(); ?>
			</article>

		</article>

	</div>

<?php get_footer(); ?>