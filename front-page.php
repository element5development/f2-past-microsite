<?php 
/*----------------------------------------------------------------*\

	HOME/FRONT PAGE TEMPLATE
	Customized home page commonly composed of various reuseable sections.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/sections/title'); ?>

<?php get_template_part('template-parts/sections/article'); ?>

<nav class="nav-years">
	<?php if (has_nav_menu('primary_navigation')) :
		wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
	endif; ?>
</nav>

<?php get_footer(); ?>