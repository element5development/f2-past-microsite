<?php 
/*----------------------------------------------------------------*\

	BLOG ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<div class="min-height">
	<div class="article-feed">
		<h2>PR & Articles</h2>
		<hr>
		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part('template-parts/preview/posts'); ?>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</div>

<?php get_footer(); ?>