<div class="gradient-border" style="background: linear-gradient(135deg, <?php the_field('color_one') ?> 25%, <?php the_field('color_two') ?> 75%);"></div>
<footer>
	<div class="block">
		<div class="left">
			<a class="brand" href="https://f2conference.com/">
				<img src="<?= get_template_directory_uri(); ?>/dist/images/F2F_Logo_White.svg" alt="Face 2 Face Entertainment Conference (F2FEC)" />
			</a>
		</div>
		<div class="right">
			<nav class="nav-footer">
				<?php if (has_nav_menu('footer_navigation')) :
					wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'nav']);
				endif; ?>
			</nav>
			<p>©Copyright <?php echo date('Y'); ?> F2FEC. All Rights Reserved.</p>
		</div>
	</div>
</footer>