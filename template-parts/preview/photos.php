<article class="gallery-preview">
	<?php if ( is_page() || is_single() ) : ?>	
		<h2>Photos</h2>
	<?php else : ?>
		<h2><?php the_title(); ?></h2>
	<?php endif; ?>
	<a href="<?php the_permalink(); ?>">
		See all photos
		<svg width="9" height="14" viewBox="0 0 9 14"><path d="M8.36 7.87l-6.43 5.29c-.56.45-1.32.3-1.71-.34a1.65 1.65 0 0 1-.22-.82v-10.58c0-.78.55-1.42 1.22-1.42.25 0 .5.09.71.26l6.43 5.29c.55.45.68 1.34.29 1.98a1.2 1.2 0 0 1-.29.34z"></path></svg>
	</a>
	<hr>
	<?php $images = get_field('gallery'); ?>
	<?php if( $images ): ?>
		<ul>
			<?php 
				$i = 0;
				foreach( $images as $image ): 
				if (++$i == 7) break;
			?>
				<li>
					<a href="<?php echo $image['url']; ?>">
						<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>
</article>