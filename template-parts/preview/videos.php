<article class="video-preview">
	<?php if ( is_page() || is_single() ) : ?>	
		<h2>Videos</h2>
	<?php else : ?>
		<h2><?php the_title(); ?></h2>
	<?php endif; ?>
	<a href="<?php the_permalink(); ?>">
		See all videos
		<svg width="9" height="14" viewBox="0 0 9 14"><path d="M8.36 7.87l-6.43 5.29c-.56.45-1.32.3-1.71-.34a1.65 1.65 0 0 1-.22-.82v-10.58c0-.78.55-1.42 1.22-1.42.25 0 .5.09.71.26l6.43 5.29c.55.45.68 1.34.29 1.98a1.2 1.2 0 0 1-.29.34z"></path></svg>
	</a>
	<hr style="background-color: <?php the_field('color_two', $page_id) ?>;">
	<?php if( have_rows('videos') ): ?>
		<ul>
			<?php while ( have_rows('videos') ) : the_row(); ?>
				<li>
					<svg width="68" height="44" viewBox="0 0 68 44">
						<path id="a" d="M27.12 9c.35 0 .72.09 1.06.29l9.54 5.5 9.53 5.51a2.11 2.11 0 0 1 0 3.67l-9.53 5.51-9.54 5.5a2.11 2.11 0 0 1-3.18-1.83v-22.03a2.11 2.11 0 0 1 2.12-2.12zm1.95 5.5v15.27l13.22-7.64-6.61-3.81z"></path>
					</svg>
					<img src="https://img.youtube.com/vi/<?php the_sub_field('youtube_id'); ?>/0.jpg" alt="video thumbnail" />
				</li>
				<?php if ( get_sub_field('youtube_id') ) { ?>
					<div class="video-lightbox">
						<div class="contents">
							<svg viewBox="0 0 75 75" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414">
								<path d="M20.1 55.9c.4.4.9.6 1.4.6.5 0 1-.2 1.4-.6l14.6-14.6 14.6 14.6c.4.4.9.6 1.4.6.5 0 1-.2 1.4-.6.8-.8.8-2 0-2.8L40.3 38.5l14.6-14.6c.8-.8.8-2 0-2.8-.8-.8-2-.8-2.8 0L37.5 35.7 22.9 21.1c-.8-.8-2-.8-2.8 0-.8.8-.8 2 0 2.8l14.6 14.6-14.6 14.6c-.8.8-.8 2 0 2.8z" fill="#fff" fill-rule="nonzero" stroke="#fff"></path>
								<path d="M59.5 73.5c7.7 0 14-6.3 14-14v-44c0-7.7-6.3-14-14-14h-44c-7.7 0-14 6.3-14 14v44c0 7.7 6.3 14 14 14h44zm-54-14v-44c0-5.5 4.5-10 10-10h44c5.5 0 10 4.5 10 10v44c0 5.5-4.5 10-10 10h-44c-5.5 0-10-4.5-10-10z" fill="#fff" fill-rule="nonzero" stroke="#fff"></path>
							</svg>
							<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php the_sub_field('youtube_id'); ?>?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
						</div>
					</div>
				<?php } ?>
			<?php endwhile; ?>
		</ul>
	<?php endif; ?>
</article>