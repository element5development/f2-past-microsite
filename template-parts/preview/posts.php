<article class="post-preview">
	<a href="<?php the_permalink(); ?>">
		<img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'thumbnail'); ?>" alt="<?php the_title(); ?>" />
		<div class="block">
			<h2><?php the_title(); ?></h2>
			<p><?php echo get_excerpt(250); ?></p>
		</div>
	</a>
</article>