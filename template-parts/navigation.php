<header>
  <div class="block">
    <a class="brand" href="https://f2conference.com/">
      <img src="<?= get_template_directory_uri(); ?>/dist/images/F2F_Logo.svg" alt="Face 2 Face Entertainment Conference (F2FEC)" />
    </a>
    <nav class="nav-primary">
      <?php if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
      endif; ?>
    </nav>
    <a class="hamburger">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30">
        <path d="M28.8 4.65v2.35a1.1 1.1 0 0 1-.34.81 1.14 1.14 0 0 1-.81.34h-25.3a1.16 1.16 0 0 1-1.15-1.15v-2.35a1.14 1.14 0 0 1 .34-.81 1.1 1.1 0 0 1 .81-.34h25.3a1.16 1.16 0 0 1 1.15 1.15zM28.46 13a1.1 1.1 0 0 1 .34.81v2.3a1.1 1.1 0 0 1-.34.81 1.14 1.14 0 0 1-.81.34h-25.3a1.16 1.16 0 0 1-1.15-1.11v-2.3a1.14 1.14 0 0 1 .34-.85 1.1 1.1 0 0 1 .81-.34h25.3a1.1 1.1 0 0 1 .81.34zM28.46 22.24a1.1 1.1 0 0 1 .34.81v2.3a1.1 1.1 0 0 1-.34.81 1.14 1.14 0 0 1-.81.34h-25.3a1.16 1.16 0 0 1-1.15-1.15v-2.35a1.14 1.14 0 0 1 .34-.81 1.1 1.1 0 0 1 .81-.34h25.3a1.1 1.1 0 0 1 .81.39z"></path>
      </svg>
    </a>
  </div>
</header>
