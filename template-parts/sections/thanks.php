<?php if ( get_field('thanks') ) : ?>
	<section class="thank-yous">
	<h2>A Special Thanks</h2>
		<?php 
			$i = 0;
			$rows = get_field('thanks');
			echo '<div class="odd-column">' ;
				foreach($rows as $row) {
					if (($i++ % 2) == 0 ) { 
						if ( $row['company'] ) {
							echo '<p class="hidden">' . $row['name']  . '<br/>'. $row['company'] .'<br/>' . $row['message'] . '</p>';
						} else {
							echo '<p class="hidden">' . $row['name']  . '<br/>' . $row['message'] . '</p>';
						}
					}
				}
			echo '</div>' ;
			$i = 0; rewind_posts();
			$rows = get_field('thanks');
			echo '<div class="even-column">' ;
				foreach($rows as $row) {
					if (($i++ % 2) != 0 ) { 
						if ( $row['company'] ) {
							echo '<p class="hidden">' . $row['name']  . '<br/>'. $row['company'] .'<br/>' . $row['message'] . '</p>';
						} else {
							echo '<p class="hidden">' . $row['name']  . '<br/>' . $row['message'] . '</p>';
						}
					}
				}
			echo '</div>' ;
		?>
	</section>
<?php endif; ?>