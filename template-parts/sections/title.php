<?php 
	$image = get_field('background_image');
?>

<section class="page-title" style="background-image: url(<?php echo $image['url']; ?>);">
	<div class="block">
		<h1><?php the_title(); ?></h1>
		<?php if ( get_field('youtube_id') ) { ?>
			<div class="title-video">
				<svg width="68" height="44" viewBox="0 0 68 44">
					<path id="b" d="M60.96 8.95a4.43 4.43 0 0 0 4.43 4.43v3.94a4.43 4.43 0 0 0 0 8.87v3.94a4.43 4.43 0 1 0 0 8.86v2.44h-63.39v-2.52a4.43 4.43 0 1 0 0-8.86v-3.94a4.43 4.43 0 0 0 0-8.87v-3.94a4.43 4.43 0 1 0 0-8.87v-2.43h63.39v2.51a4.43 4.43 0 0 0-4.43 4.44z"></path>
					<path id="a" d="M27.12 9c.35 0 .72.09 1.06.29l9.54 5.5 9.53 5.51a2.11 2.11 0 0 1 0 3.67l-9.53 5.51-9.54 5.5a2.11 2.11 0 0 1-3.18-1.83v-22.03a2.11 2.11 0 0 1 2.12-2.12zm1.95 5.5v15.27l13.22-7.64-6.61-3.81z"></path>
				</svg>
			</div>
		<?php } ?>
	</div>
	<?php if ( get_field('color_one') && get_field('color_two') ) : ?>
		<div class="title-overlay" style="background: linear-gradient(129deg, <?php the_field('color_one'); ?> 10%, <?php the_field('color_two'); ?> 100%);"></div>
	<?php endif; ?>
</section>
<?php if ( get_field('youtube_id') ) { ?>
	<div class="title-lightbox">
		<div class="contents">
			<svg viewBox="0 0 75 75" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414">
				<path d="M20.1 55.9c.4.4.9.6 1.4.6.5 0 1-.2 1.4-.6l14.6-14.6 14.6 14.6c.4.4.9.6 1.4.6.5 0 1-.2 1.4-.6.8-.8.8-2 0-2.8L40.3 38.5l14.6-14.6c.8-.8.8-2 0-2.8-.8-.8-2-.8-2.8 0L37.5 35.7 22.9 21.1c-.8-.8-2-.8-2.8 0-.8.8-.8 2 0 2.8l14.6 14.6-14.6 14.6c-.8.8-.8 2 0 2.8z" fill="#fff" fill-rule="nonzero" stroke="#fff"></path>
				<path d="M59.5 73.5c7.7 0 14-6.3 14-14v-44c0-7.7-6.3-14-14-14h-44c-7.7 0-14 6.3-14 14v44c0 7.7 6.3 14 14 14h44zm-54-14v-44c0-5.5 4.5-10 10-10h44c5.5 0 10 4.5 10 10v44c0 5.5-4.5 10-10 10h-44c-5.5 0-10-4.5-10-10z" fill="#fff" fill-rule="nonzero" stroke="#fff"></path>
			</svg>
			<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php the_field('youtube_id'); ?>?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		</div>
	</div>
<?php } ?>