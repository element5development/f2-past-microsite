<?php

/****************************************************************************
  BUTTON SHORTCODE
****************************************************************************/
function cta_button($atts, $content = null) {
  extract( shortcode_atts( array(
    'target' => '',
    'url' => '#',
    'type' => '',
  ), $atts ) );
  $link = '<a target="'.$target.'" href="'.$url.'" class="btn btn--'.$type.'"><span>' . do_shortcode($content) . $svg . '</span></a>';
  return $link;
}
add_shortcode('btn', 'cta_button');?>