$(document).ready(function () {
	var $ = jQuery;
	/*------------------------------------------------------------------
		TITLE VIDEO LIGHTBOX
	------------------------------------------------------------------*/
	$('.title-video').on('click', function () {
		$('.title-lightbox').addClass('active');
	});
	$('.title-lightbox').on('click', function () {
		var src = $(this).find('iframe').attr('src');
		$(this).find('iframe').attr('src', '');
		$(this).find('iframe').attr('src', src);
		$(this).removeClass('active');
	});
	/*------------------------------------------------------------------
		VIDEO PREVIEW LIGHTBOX
	------------------------------------------------------------------*/
	$('.video-preview ul li').on('click', function () {
		$(this).next().addClass('active');
	});
	$('.video-lightbox').on('click', function () {
		var src = $(this).find('iframe').attr('src');
		$(this).find('iframe').attr('src', '');
		$(this).find('iframe').attr('src', src);
		$(this).removeClass('active');
	});

});