$(document).ready(function () {
	var $ = jQuery;
	/*----------------------------------------------------------------*\
  	LOGIC FOR LOAD MORE BUTTON
	\*----------------------------------------------------------------*/
	if ($('.next.page-numbers').length) {
		$('.load-more').css('display', 'block');
	}
	/*----------------------------------------------------------------*\
		INFINITE SCROLL INIT
	\*----------------------------------------------------------------*/
	$('.feed').infiniteScroll({
		path: '.next.page-numbers',
		append: '.post-preview',
		button: '.load-more',
		scrollThreshold: false,
		checkLastPage: true,
		status: '.page-load-status',
	});

});