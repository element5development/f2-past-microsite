var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
			GALLERIES
	\*----------------------------------------------------------------*/
	$('.gallery-preview ul li a').featherlightGallery({
		previousIcon: '«',
		nextIcon: '»',
		galleryFadeIn: 100,
		galleryFadeOut: 300
	});
});