$(document).ready(function () {
	var $ = jQuery;
	/*------------------------------------------------------------------
		Mobile Navigation adding & removing classes
	------------------------------------------------------------------*/
	$('.hamburger').click(function () {
		$('nav.nav-primary').toggleClass('show');
	});

	function actionwindowclick(elem, action) {
		$(window).on('click', function (e) {
			if (!$(elem).is(e.target) && $(elem).has(e.target).length === 0) {
				action();
			}
		});
	}

});