$(document).ready(function () {
	var $ = jQuery;
	/*------------------------------------------------------------------
		Form input adding & removing classes
	------------------------------------------------------------------*/
	$(function () {
		$('.gfield_checkbox').closest('li').addClass('target space-out');
		$('.gfield_radio').closest('li').addClass('target space-out');
		$('select').closest('li').addClass('no-label');
	});
	$('input:not([type=checkbox]):not([type=radio])').focus(function () {
		$(this).closest('li').addClass('target');
	});
	$('textarea').focus(function () {
		$(this).closest('li').addClass('target');
	});
	$('select').focus(function () {
		$(this).addClass('target');
	});
	$('select').click(function () {
		$(this).addClass('LV_valid_field');
	});
	/*------------------------------------------------------------------
		AUTOFILL FORMS  FIX
	------------------------------------------------------------------*/
	$(function () {
		if ($('input').val() != '') {
			$(this).closest('li').addClass('target');
		}
		if ($('select').val() != '') {
			$(this).closest('li').addClass('target');
		}
		if ($('textarea').val() != '') {
			$(this).closest('li').addClass('target');
		}
	});

});