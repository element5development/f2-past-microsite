<?php 
/*----------------------------------------------------------------*\

	SINGLE GALLERY

\*----------------------------------------------------------------*/
?>
<?php get_header(); ?>

<div class="min-height">
	<article class="gallery-preview">
		<h2><?php the_title(); ?></h2>
		<hr>
		<?php $images = get_field('gallery'); ?>
		<?php if( $images ): ?>
			<ul>
				<?php foreach( $images as $image ): ?>
					<li>
						<a href="<?php echo $image['url']; ?>">
							<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	</article>
</div>

<?php get_footer(); ?>